#!/usr/bin/env bash
#SAN=$(hostname)
#echo $SAN | grep -Eo -m1 '^.+?\.'
step ca root root-ca.crt
step ca certificate "LDAP HQ" --san hm5 --san hm5.h.remmirath.com --san hq --san hq.h.remmirath.com --san ldap --san ldap.h.remmirath.com ldap.crt ldap.key -f --provisioner=lts@remmirath.com
