<?php
$pwd_min_length = 4;
$pwd_max_length = 24;

// $pwd_min_lower = 3;
// $pwd_min_upper = 1;
// $pwd_min_digit = 1;
// $pwd_min_special = 1;

// $pwd_special_chars = "^a-zA-Z0-9";
// $pwd_complexity = 4;

$pwd_no_reuse = true;

$pwd_show_policy = "always";

$hash = "SSHA";
?>
