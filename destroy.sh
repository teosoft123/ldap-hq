#!/bin/bash

docker-compose -f ldap-docker-compose.yaml down
docker image rm ldap-hq_ldap_server ldap-hq_ldap_pwd
docker volume rm ldap-config ldap-data
