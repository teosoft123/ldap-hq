FROM osixia/openldap:stable
LABEL maintainer="oleg.tsvinev@gmail.com"
# ENV LDAP_ORGANISATION="Remmirath Org"
# ENV LDAP_DOMAIN="remmirath.com"
# ENV HOSTNAME="ldap01.h.remmirath.com"

# For test and troubleshooting purposes only:
RUN apt update && apt -y install less curl vim

# These files define LDAP server evironment
COPY default.startup.yaml /container/environment/default.startup.yaml
COPY default.yaml /container/environment/default.yaml

# TODO replace these initial users and groups with LDIF files
COPY bootstrap.ldif /container/service/slapd/assets/config/bootstrap/ldif/50-bootstrap.ldif

# ADD bootstrap /container/service/slapd/assets/config/bootstrap
ADD certs /container/service/slapd/assets/certs
# ADD environment /container/environment/01-custom
